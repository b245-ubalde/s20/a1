let number;
let userWord;
let newWord = "";

function printDivisibleby5(){
	number = Number(prompt("Enter a number:"));
	console.log("This number you provided is " + number);

	for(let decrementer = number; number > 50; number--){
		if(number % 5 === 0){

			if(number % 10 === 0){
				console.log("The number is divisible by 10. Skipping this number.");
			decrementer = number;
			continue;
			}

			console.log(number);
		}
	}
}

printDivisibleby5();

function skipConsonantLetters(){
	userWord = prompt("Please enter a word:");
	userWord = userWord.toLowerCase();

	for(let incrementer = 0; incrementer < userWord.length; incrementer++){		
		if(userWord[incrementer] === "a" || userWord[incrementer] === "e" || userWord[incrementer] === "i" || userWord[incrementer] === "o" || userWord[incrementer] === "u"){
			continue;
		}
		else{
			newWord += userWord[incrementer];
		}
	}

	console.log(userWord);
	console.log(newWord);
}

skipConsonantLetters();